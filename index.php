<?php include('parts/header.php'); ?>
<body data-spy="scroll" data-target="#navbar">
<div id="top" class="page-wrap">
<?php include('parts/nav.php'); ?>
<div class="content-wrap">
  <section class="part hero">
    <div class="info">
      <h1 class="headline">GIS Programmieren</h1>
      <p class="subheadline">Gestaltung Interaktiver Systeme &ndash; Vorlesung im Sommersemester 2014</p>
    </div>
  </section>
  <section id="tasks" class="part tasks">
    <article>
      <h2>Aufgaben</h2>
      <p>Hier werden nach und nach die Aufgaben eingestellt.</p>
      <div class="grid-container">
        <div class="module"> <!-- Module Starts -->
          <figure>
            <a href="http://isabella-leon.de/" target="_blank">
              <img src="images/isabellaleon.png" alt="Aufgaben Bild: Isabella Leon">
              <figcaption>Aufgabe 1: Verlinkungen</figcaption>
            </a>
          </figure>
        </div> <!-- Module Ends -->
        <div class="module"> <!-- Module Starts -->
          <figure>
            <a href="http://cdpn.io/bwyxD" target="_blank">
              <img src="images/codepen.png" alt="Aufgaben Bild: Codepen Logo">
              <figcaption>Aufgabe 2: Layout</figcaption>
            </a>
          </figure>
        </div> <!-- Module Ends -->
        <div class="module"> <!-- Module Starts -->
          <figure>
            <a href="http://cdpn.io/gqhwt" target="_blank">
              <img src="images/codepen.png" alt="Aufgaben Bild: Codepen Logo">
              <figcaption>Aufgabe 3.1: Bilder hover</figcaption>
            </a>
          </figure>
        </div> <!-- Module Ends -->
        <div class="module"> <!-- Module Starts -->
          <figure>
            <a href="http://codepen.io/jurekbarth/full/bqgyh" target="_blank">
              <img src="images/codepen.png" alt="Aufgaben Bild: Codepen Logo">
              <figcaption>Aufgabe 3.2: JS FAQ</figcaption>
            </a>
          </figure>
        </div> <!-- Module Ends -->
		    <div class="module"> <!-- Module Starts -->
          <figure>
            <a href="http://codepen.io/jurekbarth/full/mEIbD" target="_blank">
              <img src="images/codepen.png" alt="Aufgaben Bild: Codepen Logo">
              <figcaption>Aufgabe 4: JS FAQ Neu</figcaption>
            </a>
          </figure>
        </div> <!-- Module Ends -->
        <div class="module"> <!-- Module Starts -->
          <figure>
            <a href="http://codepen.io/jurekbarth/full/qnolG/" target="_blank">
              <img src="images/codepen.png" alt="Aufgaben Bild: Codepen Logo">
              <figcaption>Aufgabe 5: Formular</figcaption>
            </a>
          </figure>
        </div> <!-- Module Ends -->
        <div class="module"> <!-- Module Starts -->
          <figure>
            <a href="http://webuser.hs-furtwangen.de/~barthjur/gis/aufgaben/aufgabe6/" target="_blank">
              <img src="images/a61.png" alt="Aufgaben Bild: Codepen Logo">
              <figcaption>Aufgabe 6.1: Formular</figcaption>
            </a>
          </figure>
        </div> <!-- Module Ends -->
        <div class="module"> <!-- Module Starts -->
          <figure>
            <a href="http://webuser.hs-furtwangen.de/~barthjur/gis/aufgaben/aufgabe6/index2.html" target="_blank">
              <img src="images/a62.png" alt="Aufgaben Bild: Codepen Logo">
              <figcaption>Aufgabe 6.2: MySQL</figcaption>
            </a>
          </figure>
        </div> <!-- Module Ends -->
        <div class="module"> <!-- Module Starts -->
          <figure>
            <a href="http://webuser.hs-furtwangen.de/~barthjur/gis/aufgaben/aufgabe7" target="_blank">
              <img src="images/a7.png" alt="Aufgaben Bild: Codepen Logo">
              <figcaption>Aufgabe 7: MySQL + Ajax</figcaption>
            </a>
          </figure>
        </div> <!-- Module Ends -->
      </div>
    </article>
  </section>
  <section id="lectures" class="lectures">
    <article>
      <h2>Programmieren</h2>
      <p>In Programmieren haben wir einen Einblick in die Java Welt bekommen. Mit Processing haben wir wöchtentlich super Aufgaben gelöst. Zur Krönung haben wir ein Spiel programmiert.</p>
    </article>
    <article>
      <h2>Allgemine BWL</h2>
      <p>In BWL haben wir anschaulich erklärt bekommen, wie unsere Wirtschaft so funktioniert. Durch "Würschtlbuden" wurden die schwierigen Probleme auf eine verständliche Ebene zurückgeholt.</p>
    </article>
    <article>
      <h2>GIS</h2>
      <p>In Gis werden wir lernen, wie wir Webseiten programmieren und interaktiv gestalten. Schade, dass wir nicht über Versionierung lernen. Der Javascript-Teil wird wohl am interessantesten. </p>
    </article>
    <article>
      <h2>AV Produktion</h2>
      <p>In AV Produktion werden wir einen kleinen Kurzfilm drehen. Freue mich jetzt schon auf das Ergebnis.</p>
    </article>
  </section>
  <section id="about" class="part aboutme">
    <article>
      <div class="text">
        <h1>Hallo ich bins :)</h1>
        <p>Ich studiere im zweiten Semster Online Medien. Bin geboren, zur Schule gegangen, habe mein Abi gemacht und wohne in Freiburg, nachdem ich für zwei kurze Jahre in München an der TU war.</p>
      </div>
      <div class="aside">
        <img src="images/me.jpg" alt="Picture of me">
      </div>
    </article>
  </section>

</div>
<?php include('parts/footer.php'); ?>
