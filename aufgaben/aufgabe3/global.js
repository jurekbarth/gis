function toggleClassNext() {
  this.nextElementSibling.classList.toggle('active');
}

function toggleClassOnClick(className) {
  var classList = document.getElementsByTagName(className);
  for(var i=0;i<classList.length;i++){
    classList[i].addEventListener('click', toggleClassNext, false);
  }
}

window.onload = function() {
 toggleClassOnClick("dt");
};