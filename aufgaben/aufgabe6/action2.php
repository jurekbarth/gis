<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Form</title>
  <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="style.css">
</head>
<body>

<?php
$db = new mysqli('141.28.120.50', 'TestUser', 'TestGIS', 'TestDB');
if($db->connect_errno > 0) {
  ?>
  <div class="container">
    <p>
      <?php
        die('Unable to connect to database [' . $db->connect_error . ']');
      ?>
    </p>
  </div>
<?php } ?>

<?php
$query = $_POST['suche'];
//$sql = "SELECT * FROM 'teilnehmer' WHERE name LIKE '%$query%' LIMIT 0,50";
$sql = <<<SQL
    SELECT *
    FROM `teilnehmer`
    WHERE `name` LIKE '%$query%'
    limit 0,50
SQL;
if(!$result = $db->query($sql)){
    die('There was an error running the query [' . $db->error . ']');
}
?>


<div class="container big">
  <p class="white">Folgende Einträge konnten wir finden:</p>
  <table>
  <tr>
    <th>Kennnummer</th>
    <th>Name</th>
    <th>Straße</th>
    <th>PLZ</th>
    <th>Telefon</th>
  </tr>
    <?php
      while($row = $result->fetch_assoc()){
    ?>
      <tr>
        <td><?php echo $row['Kennummer']; ?></td>
        <td><?php echo $row['Name']; ?></td>
        <td><?php echo $row['Strasse']; ?></td>
        <td><?php echo $row['plz']; ?></td>
        <td><?php echo $row['Telefon']; ?></td>
      </tr>
    <?php } ?>
  </table>
</div>
<?php $db->close(); ?>
</body>
</html>
