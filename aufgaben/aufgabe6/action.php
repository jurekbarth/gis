<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Form</title>
  <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="container">
  <p class="white">Vielen Dank wir haben folgende Nachricht erhalten:</p>
  <p><span class="white">Ihr Name:</span> <?php echo $_POST['anrede']; ?> <?php echo $_POST['vorname']; ?> <?php echo $_POST['name']; ?></p>
  <p><span class="white">Ihre Emailadresse:</span> <?php echo $_POST['mail']; ?></p>
  <p><span class="white">Ihre Nachricht an uns:</span> <br><?php echo $_POST['textarea']; ?></p>
</div>
</body>
</html>