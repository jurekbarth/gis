<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Flugsuche</title>
</head>
<body>
<?php include("config.php"); ?>
  <div class="container">
<?php
$sql = <<<SQL
  SELECT *
  FROM `flughafen`
SQL;
  if(!$result = $db->query($sql)){
      die('There was an error running the query [' . $db->error . ']');
  }
  if(!$ziel = $db->query($sql)){
      die('There was an error running the query [' . $db->error . ']');
  }
?>

    <form action="flysearch.php" method="post">
      <label for="startflughafen">Startflughafen</label>
      <select name="startflughafen" id="startflughafen">
          <?php
            while($row = $result->fetch_assoc()){
          ?>
            <option value="<?php echo $row['idflughafen']; ?>"><?php echo $row['stadtname']; echo " ("; echo $row['idflughafen']; echo ")"; ?></option>
          <?php } ?>
      </select>
      <br>
      <label for="zielflughafen">Zielflughafen</label>
      <select name="zielflughafen" id="zielflughafen">
          <?php
            while($row = $ziel->fetch_assoc()){
          ?>
            <option value="<?php echo $row['idflughafen']; ?>"><?php echo $row['stadtname']; echo " ("; echo $row['idflughafen']; echo ")"; ?></option>
          <?php } ?>
      </select>
      <br>
      <label for="startdate">Startdatum</label>
      <input name="startdate" type="date" value=""/>
      <label for="enddate">Enddatum</label>
      <input name="enddate" type="date" value=""/>
      <br>
      <a href="#" id="hotel">Hotels in der Nähe suchen</a>
      <div id="hotellist"></div>
      <br>
      <input type="submit" value="Submit" />
    </form>
  </div>
<?php $db->close(); ?>

<a href="index.txt">Code index.php</a>
<a href="config.txt">Code config.php</a>
<a href="flysearch.txt">Code flysearch.php</a>
<a href="hotel.txt">Code hotel.php</a>
<a href="javascript.txt">Code javascript.js</a>
<script src="javascript.js"></script>
</body>
</html>