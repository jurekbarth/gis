<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Ihre Flüge</title>
</head>
<body>
<?php include("config.php");
$start = $_POST['startflughafen'];
$ziel = $_POST['zielflughafen'];
$sql = <<<SQL
  SELECT *
  FROM `flug`
  WHERE `start` = '$start' AND `ziel` = '$ziel'
SQL;
//   WHERE `idflughafen` = '$query'
  if(!$result = $db->query($sql)){
      die('There was an error running the query [' . $db->error . ']');
  }
?>

<h1>Hier ihre Flüge</h1>

<table>
  <tr>
    <th>Flug ID</th>
    <th>Start</th>
    <th>Ziel</th>
    <th>Datum</th>
    <th>Flug Nummer</th>
  </tr>
    <?php
      while($row = $result->fetch_assoc()){
    ?>
      <tr>
        <td><?php echo $row['idflug']; ?></td>
        <td><?php echo $row['start']; ?></td>
        <td><?php echo $row['ziel']; ?></td>
        <td><?php echo $row['datum']; ?></td>
        <td><?php echo $row['flugnr']; ?></td>
      </tr>
    <?php } ?>
  </table>

</body>
</html>
