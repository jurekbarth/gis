<header class="main-header">
  <a class="h1 logo" href="#top">Jurek Barth</a>
  <nav id="navbar">
    <ul class="main-nav nav menu">
      <li><a href="#top">Home</a></li>
      <li><a href="#tasks">Aufgaben</a></li>
      <li><a href="#lectures">Vorlesungen</a></li>
      <li><a href="#about">About</a></li>
    </ul>
  </nav>
</header>